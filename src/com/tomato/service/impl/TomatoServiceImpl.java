package com.tomato.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.tomato.bean.ItemCountBean;
import com.tomato.dao.TomatoDao;
import com.tomato.dao.impl.TomatoDaoImpl;
import com.tomato.service.TomatoService;

public class TomatoServiceImpl implements TomatoService{

	@Override
	public void getIssueDetailForPreviousDays() {
		
	}

	@Override
	public List<Integer> getDstinctRestaurentId() {
		
		TomatoDao dao = new TomatoDaoImpl();
		List<Integer> restaurantIdList = dao.getDstinctRestaurentId();
		if(restaurantIdList != null && restaurantIdList.size() > 0){
			Iterator<Integer> itr = restaurantIdList.iterator();
			while(itr.hasNext()){
				int restaurantId = itr.next();
				List<ItemCountBean> list = dao.getItemCountByRestaurantId(restaurantId);
				Map<Integer, List<ItemCountBean>> mapData = new HashMap<>();
				Iterator<ItemCountBean> itrList = list.iterator();
				while(itrList.hasNext()){
					ItemCountBean icb = itrList.next();
					if(mapData.containsKey(icb.getItemId())){
						mapData.get(icb.getItemId()).add(icb);
					}
					else{
						ArrayList<ItemCountBean> al = new ArrayList<>();
						al.add(icb);
						mapData.put(icb.getItemId(), al);
					}
				}
				
				Iterator<Integer> itrMap = mapData.keySet().iterator();
				while(itrMap.hasNext()){
					int itemId = itrMap.next();
					int count = mapData.get(itemId).size();
					if(count > 4){
						dao.disableItemById(itemId);
					}
				}
				
			}
		}
		return null;
	}
	
}
