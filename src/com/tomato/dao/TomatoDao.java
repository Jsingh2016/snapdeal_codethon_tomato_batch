package com.tomato.dao;

import java.util.List;

import com.tomato.bean.ItemCountBean;
import com.tomato.bean.RestaurantPeakHoursBean;

public interface TomatoDao {

	public void getIssueDetailForPreviousDays();
	
	String DistinctRestaurentId = "select distinct(restaurant_id) from issue_detail where CAST(time_of_incedent AS DATE) > now()-4 ";
	public List<Integer> getDstinctRestaurentId(); 
	
	String ItemCountByRestaurantId = "select reason_id, item_id, count(*) itemCount from isue_detail group by "
			+ " item_id, CAST(time_of_incedent AS DATE) where CAST(time_of_incedent AS DATE) > now()-4 and restaurant_id = ?";
	public List<ItemCountBean> getItemCountByRestaurantId(int restaurantId);
	
	String QueryForSeverity = "select i.reason_id, i.item_id, count(*) itemCount from isue_detail i, restaurant_detail r group by "
			+ " item_id, CAST(time_of_incedent AS DATE) where CAST(time_of_incedent AS DATE) > now()-4 and i.time_of_incedent between ? and ? and r.restaurant_id = ?";
	
	String PeakHoursQuery = "select * from restaurant_peak_hours where restaurant_id=?";
	public List<RestaurantPeakHoursBean> getPeakHoursByRestaurantId(int restaurantId);
	
	String DeiableItemQuery = "update item_list set isItemActive = 'N' where item_id=?";
	public void disableItemById(int itemId);
	
}
