package com.tomato.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.tomato.bean.ItemCountBean;
import com.tomato.bean.RestaurantPeakHoursBean;
import com.tomato.dao.TomatoDao;
import com.tomato.util.ConnectionUtil;

public class TomatoDaoImpl implements TomatoDao{

	@Override
	public void getIssueDetailForPreviousDays() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Integer> getDstinctRestaurentId() {
		
		List<Integer> distinctRestaurantIdList = new ArrayList<>();
		
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			connection = ConnectionUtil.getConnection();
			pstmt = connection.prepareStatement(DistinctRestaurentId);
			rs = pstmt.executeQuery();
			while(rs.next()){
				distinctRestaurantIdList.add(rs.getInt(1));
			}
		}
		catch(Exception e){
			System.out.println(e);
		}
		finally{
			ConnectionUtil.closeResultSet(rs);
			ConnectionUtil.closePreparedStatement(pstmt);
			ConnectionUtil.closeConnection(connection);
		}
		
		return distinctRestaurantIdList;
	}

	@Override
	public List<ItemCountBean> getItemCountByRestaurantId(int restaurantId) {
		List<ItemCountBean> distinctRestaurantIdList = new ArrayList<>();
		
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			connection = ConnectionUtil.getConnection();
			pstmt = connection.prepareStatement(ItemCountByRestaurantId);
			pstmt.setInt(1, restaurantId);
			rs = pstmt.executeQuery();
			while(rs.next()){
				ItemCountBean icb = new ItemCountBean();
				icb.setCount(rs.getInt("itemCount"));
				icb.setIssueDate(rs.getDate(""));
				icb.setItemId(rs.getInt("item_id"));
			}
		}
		catch(Exception e){
			System.out.println(e);
		}
		finally{
			ConnectionUtil.closeResultSet(rs);
			ConnectionUtil.closePreparedStatement(pstmt);
			ConnectionUtil.closeConnection(connection);
		}
		
		return distinctRestaurantIdList;
	}

	public void calculateSeverity(int restaurantId){
		//List<RestaurantPeakHoursBean> distinctRestaurantIdList = new ArrayList<>();
		
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			connection = ConnectionUtil.getConnection();
			pstmt = connection.prepareStatement(ItemCountByRestaurantId);
			pstmt.setInt(1, restaurantId);
			rs = pstmt.executeQuery();
			while(rs.next()){
				ItemCountBean icb = new ItemCountBean();
				icb.setCount(rs.getInt("itemCount"));
				icb.setIssueDate(rs.getDate(""));
				icb.setItemId(rs.getInt("item_id"));
			}
		}
		catch(Exception e){
			System.out.println(e);
		}
		finally{
			ConnectionUtil.closeResultSet(rs);
			ConnectionUtil.closePreparedStatement(pstmt);
			ConnectionUtil.closeConnection(connection);
		}
		
		//return distinctRestaurantIdList;
	}

	@Override
	public List<RestaurantPeakHoursBean> getPeakHoursByRestaurantId(int restaurantId) {
		List<RestaurantPeakHoursBean> distinctRestaurantIdList = new ArrayList<>();
		
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			connection = ConnectionUtil.getConnection();
			pstmt = connection.prepareStatement(PeakHoursQuery);
			pstmt.setInt(1, restaurantId);
			rs = pstmt.executeQuery();
			while(rs.next()){
				ItemCountBean icb = new ItemCountBean();
				icb.setCount(rs.getInt("itemCount"));
				icb.setIssueDate(rs.getDate(""));
				icb.setItemId(rs.getInt("item_id"));
			}
		}
		catch(Exception e){
			System.out.println(e);
		}
		finally{
			ConnectionUtil.closeResultSet(rs);
			ConnectionUtil.closePreparedStatement(pstmt);
			ConnectionUtil.closeConnection(connection);
		}
		
		return distinctRestaurantIdList;
	}

	@Override
	public void disableItemById(int itemId) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		try{
			connection = ConnectionUtil.getConnection();
			pstmt = connection.prepareStatement(DeiableItemQuery);
			pstmt.setInt(1, itemId);
			pstmt.executeUpdate();	
		}
		catch(Exception e){
			System.out.println(e);
		}
		finally{
			ConnectionUtil.closePreparedStatement(pstmt);
			ConnectionUtil.closeConnection(connection);
		}
	}
	
}
